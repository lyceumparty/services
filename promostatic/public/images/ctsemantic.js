function triggerMobileSidebar() {
	$('#mobiSB').sidebar('toggle');
}

function updateClubStatus(clubId) {
	let oBtn = document.getElementById('joinInButton');
	
	$.get('/auth/join_club?id=' + clubId, function(data) {
		if (data != "absent") {
			oBtn.innerHTML = 'Перейти к моим клубам';
			oBtn.classList.remove('green');
			oBtn.href = "/auth/continue?tgt=clubs";
		}
	}).fail(function() { console.log('Club update failed :/'); });
}