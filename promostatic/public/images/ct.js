var _mobileSubmenuStatus = false;

function toggleMobileSubmenu() {
	_mobileSubmenuStatus = !_mobileSubmenuStatus;
	
	let newStatus = (_mobileSubmenuStatus) ? 'block' : 'none';
	document.getElementById('mnvb').style.display = newStatus;
}

function filterNewsContent(query, ids) {
	let aquery = query.toLowerCase();
	
	for (let i = 0; i < ids.length; i++) {
		let newsId = ids[i];
		let el = document.getElementById('newsrow_' + newsId);
		let headerEl = document.getElementById('newsheader_' + newsId);
		let tagEl = document.getElementById('newstag_' + newsId);
		
		if (query == '' || headerEl.innerHTML.toLowerCase().includes(aquery) || tagEl.innerHTML.toLowerCase().includes(aquery))
			el.style.display = 'block';
		else
			el.style.display = 'none';
	}
}