#!/usr/bin/env rake
module CT
	SEMANTIC_TARGET = File.join(__dir__, 'promostatic', 'public', 'semanticui')
	
	JQUERY_TARGET = File.join(__dir__, 'promostatic', 'public', 'jquery.min.js')
	JQUERY_LATEST = 'https://cdn.jsdelivr.net/npm/jquery/dist/jquery.min.js'
	
	DARKLY_PATH = File.join(__dir__, '3rdparty', 'semanticui-forest-themes', 'dist', 'bootswatch', 'v4')
end

def run_server(state, port = 9292)
	sh 'puma', '-e', state, '-p', (ENV['CAVETOWN_PORT'] || 9292).to_s
end

task :shell do
	require_relative 'cavetown'
	require 'irb'
	binding.irb
end

desc "Run IRB shell in CMS context"
task :irb => :shell

desc "Clean source directory"
task :clean do
	[CT::SEMANTIC_TARGET, CT::JQUERY_TARGET].each do |dd|
		rm_rf dd
	end
end

task :distclean => :clean

task :fetch_jquery do
	puts "downloading jquery from #{CT::JQUERY_LATEST}"
	# TODO: make more portable
	sh 'curl', '-kLo', CT::JQUERY_TARGET, CT::JQUERY_LATEST
end

task :pack_semantic do
	mkdir_p(CT::SEMANTIC_TARGET) if not File.directory? CT::SEMANTIC_TARGET
	
	['themes', 'semantic.min.css', 'semantic.min.js'].each do |migr|
		cp_r File.join(__dir__, '3rdparty', 'semanticui', migr), File.join(CT::SEMANTIC_TARGET, migr)
	end
	
	darkly_name = 'semantic.darkly.min.css'
	cp File.join(CT::DARKLY_PATH, darkly_name), File.join(CT::SEMANTIC_TARGET, darkly_name)
end

desc "Run CaveTown CMS in development mode"
task :run => [:fetch_jquery, :pack_semantic] do
	run_server(ENV['CAVETOWN_STATE'] || 'development')
end

task :production => [:fetch_jquery, :pack_semantic] do
	run_server('production')
end

desc "Run CaveTown CMS in production mode"
task :prod => :production

task :default => :run
