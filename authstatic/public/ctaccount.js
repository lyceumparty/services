function verifySwearing(text) {
	if (window.containsMat(text)) {
		window.alert('Пожалуйста, воздержитесь от употребления ненормативной лексики.');
		return true;
	}
	
	return false;
}

//
// Example:
// postToURL("/auth/continue", { id: 12, type: "debugauth" }, 
//			 function(success, response) { console.log((success ? "yes" : "no") + " " + response); } );
//

function postToURL(url, what, cb) {
	let request = new XMLHttpRequest();
	request.open('POST', url);
	
	if ((typeof what) == "object") {
		request.setRequestHeader('Content-type', 'application/json');
		request.send(JSON.stringify(what));
	} else
		request.send(what);
		
	request.onreadystatechange = function() {
		if (request.readyState === XMLHttpRequest.DONE)
			cb((request.status == 200), request.responseText);
	}
}

function postIDAuth(el, afterwards) {
	let id = el.value;
	el.value = ""; // empty it
	
	const data = { id: id, type: "debugauth" };
	
	postToURL('continue', data, function(success, token) {
		if (success) {
			document.cookie = "CT_AUTH=" + token;
			
			window.alert('success');
			window.location.href = afterwards;
		} else
			document.getElementById('authErrorLabel').style.visibility = 'visible';
	});
}

function areYouSure(text = "Вы уверены?") {
	return window.confirm(text);
}

function adminDeleteAccount(target) {
	if (areYouSure('Вы уверены, что хотите удалить пользователя ' + target + '? Эта операция безвозвратна.')) {
		postToURL('manage', { adminAction: true, what: 'delete', target: target }, function(success, response) {
			if (!success)
				window.alert('Произошла ошибка. ' + response);
			else {
				document.getElementById('userrow_' + target).style.display = 'none';
				window.alert('Успех!');
			}
		});
	}
}

function adminDeleteArticle(articleId) {
	if (areYouSure('Вы уверены? После совершения этой операции эту статью буде невозможно восстановить.')) {
		postToURL('edit_article', { id: articleId, doDelete: true }, function(success, response) {
			if (!success)
				window.alert('Произошла ошибка. ' + response);
			else {
				document.getElementById('articlerow_' + articleId).style.display = 'none';
				window.alert('Успех!');
			}
		})
	}
}

function adminChangeRole(target, whichRole, oldRole) {
	postToURL('manage', { adminAction: true, what: 'changeRole', target: target, role: whichRole }, function(success, response) {
		if (!success) {
			document.getElementById('userrole_' + target).value = oldRole;
			window.alert("Произошла ошибка. " + response);
		} else
			window.alert("Успех!");
	});
}

function adminAcceptRejectApplication(whatToDo, target) {
	if (whatToDo == 'reject') {
		if (!areYouSure())
			return;
	} 
	
	postToURL('manage', { adminAction: true, what: 'setApplicationStatus', target: target, status: (whatToDo != 'reject') }, function(success, response) {
		if (!success)
			window.alert('Произошла ошибка. ' + response);
		else {
			let elApplicationRow = document.getElementById('applicationrow_' + target);
			let elUserRoleRow = document.getElementById('userrole_' + target);
			
			if (whatToDo != 'reject')
				elUserRoleRow.value = 'member';
			
			elApplicationRow.style.display = 'none';
		}
	});
}

function postJoinForm() {
	const data = { name: document.getElementById('nameValue').value,
				   grade: document.getElementById('classValue').value,
				   description: document.getElementById('whyThoValue').value };
	
	// if this succeeds, then we need to ask the user to censor everything first
	if (verifySwearing(data.name) || verifySwearing(data.description))
		return;
	
	postToURL('partyform', data, function(success, response) {
		console.log("response: " + response);
		
		if (success)
			window.location.reload();
		else
			window.alert(response);
	});
}

function postNewIdea(contents) {
	if (verifySwearing(contents))
		return;
		
	postToURL('make_idea', contents, function(success, response) {
		if (!success)
			window.alert('Произошла ошибка. ' + response);
		else {
			window.alert('Успех!');
			window.location.href = "ideas";
		}
	})
}

function hoverableMouseHoverEvent(el, on) {
	let resourceOn = el.getAttribute('ct-picker-on');
	let resourceOff = el.getAttribute('ct-picker-off');
	
	if (on)
		el.setAttribute('src', resourceOn);
	else
		el.setAttribute('src', resourceOff);
}

function hoverableMouseClickEvent(el) {
	let where = el.getAttribute('ct-href');
	
	if (where != '#js-eval')
		window.location.href = where;
	else {
		let what = el.getAttribute('ct-js-eval');
		eval(what);
	}
}

function showInfoClubBlock() {
	let el = document.getElementById('infoForClubBlock');
	
	if (el.style.display == 'none')
		el.style.display = 'inherit';
	else
		el.style.display = 'none';
}

function postClubInfo(clubId) {
	let payload = {
		id: clubId, 
		action: 'save_params', 
		name: document.getElementById('nameValue').value,
		description: document.getElementById('descriptionValue').value,
		allow_registration: document.getElementById('registrationOpenValue').checked,
		hidden: document.getElementById('hiddenValue').checked,
		join_link: document.getElementById('connectLinkValue').value.trim()
	};
	
	if (payload.name.trim().length < 1) {
		window.alert('Имя клуба не может быть пустым!');
		return;
	}
	
	postToURL('clubs', payload, function(success, response) {
		if (!success) {
			window.alert('Произошла ошибка. ' + response);
			return;
		}
		
		document.getElementById('infoForClubBlock').style.display = 'none';
		document.getElementById('nameHeading').innerHTML = payload.name;
		window.alert('Успех!');
	});
}

function postVote(ideaId, votedFor) {
	postToURL('make_vote', { id: ideaId, triggerState: (votedFor ? 'like' : 'dislike') }, function(success, response) {
		if (!success) {
			window.alert('Произошла ошибка. ' + response);
			return;
		}
	})
}

function performLeaveClub(clubId) {
	if (!areYouSure())
		return;
		
	postToURL('join_club', { id: clubId, state: false }, function(success, response) {
		if (!success) {
			window.alert("Произошла ошибка. " + response);
			return;
		}
		
		document.getElementById('relatedClubItem_' + clubId).style.display = 'none';
		window.alert('Успех!');
	})
}

function performPromoteClubMember(clubId, memberId) {
	if (!areYouSure())
		return;
		
	postToURL('clubs', { id: clubId, action: 'promote', who: memberId }, function(success, response) {
		if (!success) {
			window.alert("Произошла ошибка. " + response);
			return;
		}
		
		document.getElementById('userStatusInClub_' + memberId).innerHTML = 'Администратор';
		window.alert('Успех!');
	});
}

function performDeleteClubMember(clubId, memberId) {
	if (!areYouSure())
		return;
		
	postToURL('clubs', { id: clubId, action: 'kick', who: memberId }, function(success, response) {
		if (!success) {
			window.alert("Произошла ошибка. " + response);
			return;
		}
		
		document.getElementById('userInClub_' + memberId).style.display = 'none';
		window.alert('Успех!');
	})
}

function performDeleteClub(clubId) {
	if (!window.confirm('Вы уверены?'))
		return;
		
	postToURL('clubs', { id: clubId, action: 'please_delete' }, function(success, response) {
		if (!success) {
			window.alert("Произошла ошибка. " + response);
			return;
		}
		
		window.location.href = "clubs";
	})
}

function performUploadClubIcon(clubId, uploaderElement) {
	let reader = new FileReader();
	
	reader.addEventListener('load', function() {
		postToURL('clubs', { id: clubId, action: 'change_icon', value: reader.result }, function(success, response) {
			if (!success) {
				window.alert("Произошла ошибка. " + response);
				return;
			}
			
			document.getElementById('avatarDisplayElement').src = reader.result;
		});
	});
	
	let file = uploaderElement.files[0];
	if (file)
		reader.readAsDataURL(file);
}