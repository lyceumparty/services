#!/usr/bin/env ruby
# CaveTown CMS global values
require 'toml'

module CT
	# web app root folder
	ROOT = File.absolute_path(File.join(File.dirname(__FILE__), '..'))

	# web app config
	CONFIG_PATH = File.join(ROOT, 'config.toml')
	raise("#{CONFIG_PATH} is required to be present") if not File.exists? CONFIG_PATH

	CONFIG = TOML.load_file(CONFIG_PATH)

	# module static paths
	PROMO_COMPONENTS_ROOT = File.join(ROOT, 'promostatic')
	PROMO_STATIC_ROOT = File.join(PROMO_COMPONENTS_ROOT, 'public')
	PROMO_VIEWS_DIRECTORY = File.join(PROMO_COMPONENTS_ROOT, 'views')
	
	AUTH_ROOT = File.join(ROOT, 'authstatic')
	
	# deactivated user (banned user) blacklist file
	BLACKLIST_PATH = File.join(ROOT, 'blacklist.txt')
	File.write(BLACKLIST_PATH, 'dbg') if not File.exists? BLACKLIST_PATH
	
	BLACKLISTED_USERS = File.read(BLACKLIST_PATH).strip.split(',')
	
	# Clicky analytics ID taken from the config
	CLICKY_ID = CONFIG['general']['clicky_analytics']
	ALLOW_DARK_THEME = CONFIG['general']['allow_dark_theme'] || false
end
