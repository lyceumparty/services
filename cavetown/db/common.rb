#!/usr/bin/env ruby
require 'openssl'
require 'dalli'
require 'mongoid'

class Integer
	def hours
		return (self * 60 * 60) # in seconds
	end

	alias hour hours
end

module CT
	def self.make_hash(str)
		hash = OpenSSL::Digest::SHA512.new
		hash << str
		return hash.hexdigest
	end

	MEMCACHED = Dalli::Client.new("#{CONFIG['memcached']['host']}:#{CONFIG['memcached']['port']}",
				      { :namespace => CONFIG['memcached']['namespace'],
				        :compress => true })

	# setup Mongoid for MongoDB access
	Mongoid.configure do |cf|
		cf.clients.default = { 
					:hosts => [ "#{CONFIG['mongo']['host']}:#{CONFIG['mongo']['port']}" ],
					:database => CONFIG['mongo']['db'],
					:options => {
							:user => CONFIG['mongo']['user'],
							:password => CONFIG['mongo']['password'],
							:roles => [ 'dbAdmin' ]
						    }
				     }
		
		cf.log_level = :debug
	end
	puts 'mongoid success'
end
