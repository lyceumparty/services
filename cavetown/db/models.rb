#!/usr/bin/env ruby
require 'mongoid'
require_relative 'common'

# Mongoid models

module CT
	module Models
		class User
			include Mongoid::Document

			# VK user ID
			field :vk_id, :type => String

			# party application form 
			field :application, :type => Hash

			# user's role in party (:regular, :member, :deputy, :club_admin)
			field :role, :type => Symbol
			
			# optional syncable information
			field :full_name, :type => String
			field :page_link, :type => String
			field :photo_url, :type => String
			
			validates_presence_of :vk_id, :application, :role
		end
		
		class NewsArticle
			include Mongoid::Document
			
			# date added
			field :when_added, :type => DateTime
			# date updated
			field :when_updated, :type => DateTime
			
			# page title
			field :title, :type => String
			# page tagline
			field :tagline, :type => String
			# page contents (Markdown)
			field :contents, :type => String
			
			# page author
			field :author_id, :type => String
			
			# thumbnail image
			field :thumbnail_link, :type => String
			
			validates_presence_of :when_added, :title, :contents, :author_id
		end
		
		class Idea
			include Mongoid::Document
			
			# date added
			field :when_added, :type => DateTime
			
			# idea name
			field :name, :type => String
			# idea description (optional)
			field :description, :type => String
			
			# idea state (:brand_new, :reviewed, :rejected, :implemented)
			field :state, :type => Symbol
			# deputy ID who decided to bring up this idea later on
			field :state_setter_id, :type => String
			
			# idea author
			field :author_id, :type => String
			
			# people, who support the idea
			field :support_ids, :type => Array
			# people, who don't
			field :against_ids, :type => Array
			
			validates_presence_of :when_added, :name, :description, :state
		end
		
		class Club
			include Mongoid::Document
			
			# display name
			field :name, :type => String
			# icon
			field :icon_url, :type => String
			
			# founder ids
			field :founders, :type => Array
			# members (IDs)
			field :users, :type => Array
			
			# how to join (:on_site, :contact_link, :closed)
			field :join_type, :type => Symbol
			# join link
			field :join_link, :type => String
			
			# current description
			field :description, :type => String
			
			# is hidden
			field :hidden, :type => Boolean
			
			validates_presence_of :name, :founders, :users, :join_type
		end
	end
end
