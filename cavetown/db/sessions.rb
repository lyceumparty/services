#!/usr/bin/env ruby
require 'dalli'
require 'time'
require_relative 'common'

module CT
	class Session
		attr_reader :token
	
		def initialize(account_id, finish_in=2.hours)
			if not account_id.nil?
				@token = CT.make_hash("#{account_id}+#{Time.now.to_i}+#{@finish.to_i}")

				CT::MEMCACHED.set(@token, account_id, finish_in)
			else
				if finish_in.is_a? String
					# existing token import
					@token = finish_in
				else
					# invalid empty session
					@token = "<INVALID>"
				end
			end
		end

		def self.existing_token(token)
			raise('Session has expired or is invalid.') if CT::MEMCACHED.get(token).nil?
			
			return Session.new(nil, token)
		end

		def is_actually
			return CT::MEMCACHED.get(@token)
		end
		
		def valid?
			if is_actually.nil?
				return false
			end

			return true
		end

		def finish
			CT::MEMCACHED.delete(@token)
		end
	end
end

