#!/usr/bin/env ruby
require 'sinatra'
require 'json'
require 'erubis'
require_relative 'common'

module CT
	module Apps
		class Auth < Sinatra::Base
			ROLE_MAP = { :regular => "Не является участником партии",
					 	  	 :member => "Участник партии",
					  	    :club_admin => "Модератор клуба",
					  	    :deputy => "Кандитат от партии на выборах" }
			
			ALLOWED_ADMIN_ROLES = [ :member, :deputy ]
			
			# gets current user info (DB document)
			def get_user_for_current_session(request)
				begin
					session = Session.existing_token(request.cookies["CT_AUTH"] || "")
					if session.valid?
						current_id = session.is_actually
						user = CT::Models::User.where( :vk_id => current_id ).first
						
						return user
					end
				rescue => ex
					puts ex
				end

				return nil
			end
			
			# detect user photo URL
			def nice_user_photo(user)
				if user.nil? or user.photo_url.nil?
					return "images/empty.png"
				else
					return user.photo_url.to_s
				end
			end
			
			# detect user-friendly user auth method name from ID
			def nice_auth_method_string_for_user(user)
				value = user.vk_id.to_s
				
				if value.start_with? 'tlg'
					return 'Telegram'
				elsif value.start_with? 'dbg'
					return 'Отладочный пользователь'
				else
					return 'ВКонтакте'
				end
			end
			
			# detect user-friendly user role from DB document
			def nice_role_string_for_user(user)
				value = user.role
				return (ROLE_MAP[value] || ROLE_MAP[:regular])
			end
			
			def is_vk_user?(str)
				# TODO: rewrite without using hacks
				['tlg', 'dbg'].each do |prefix|
					if str.start_with? prefix
						return false
					end
				end
				
				return true
			end
			
			def is_blacklisted?(user_id)
				return CT::BLACKLISTED_USERS.include? user_id
			end
			
			# add the user to 'blacklist.txt' (deactivation)
			def blacklist_user(user_id)
				return if is_blacklisted? user_id
				
				begin
					CT::BLACKLISTED_USERS.push(user_id)
					File.write(CT::BLACKLIST_PATH, CT::BLACKLISTED_USERS.join(','))
				rescue
					puts "warning! blacklisting #{user_id} failed, probably file permissions are incorrect"
				end
			end
			
			def generate_t_me_link(username)
				trimmed_username = if username.start_with? '@'
												username[1...]
										 else
												username
										 end
										 
				return "https://t.me/#{trimmed_username}" 
			end
			
			# sync user info with authorization providers
			def update_user_info_during_auth(auth_id, type, payload)
				if is_blacklisted? auth_id
					return nil # no sync or creation needed
				end
				
				real_type = if not type.is_a? Symbol
									type.to_s.to_sym
								else
									type
								end
								
				# find the user in the DB first
				user = CT::Models::User.where( :vk_id => auth_id ).first
				if user.nil?
					# create one
					
					user = CT::Models::User.new
					user.vk_id = auth_id
					user.application = { :sent => false, :rejected => false }
					user.role = :regular
				end
				
				case real_type
				when :vkauth
					sub_user = payload[:params]['session']['user']
					
					# update VK page link
					user.page_link = sub_user['href']
					user.full_name = [ sub_user['first_name'], sub_user['last_name'] ].join(' ')
				when :tgauth
					# generate t.me link from username
					user.page_link = generate_t_me_link(payload[:params]['username'] || "@id#{payload[:params]['id']}")
					user.full_name = [ payload[:params]['first_name'], payload[:params]['last_name'] ].join(' ')
					user.photo_url = payload[:params]['photo_url']
				else
					user.page_link = '<unavailable>'
					user.full_name = 'For testing purposes only'
				end
				
				user.save
				return user
			end
			
			# componentize article markdown contents
			def componentize_article_contents(article)
				result = { :title => nil,
					 		  :tagline => nil, 
						  	  :contents => nil }
				
				# get just the markdown lines
				article_split = article.gsub("<br>", "").split("\n").reject { |item| item.strip.empty? }
				
				if article_split.first.start_with? '# '
					# this is the heading
					heading = article_split.shift[2...]
					result[:title] = heading
				end
				
				if article_split.first.is_italic?
					result[:tagline] = article_split.shift[1...][...-1]
				end
				
				result[:contents] = article_split.join("<br><br>\n")
				
				if result[:title].nil? 
					result[:title] = result[:contents].split('.').first + '.'
					result[:tagline] = result[:contents].first_n_words(256)
				end
				
				return result
			end
			
			# lists all clubs the user is part of
			def detect_clubs_for_user(user)
				# TODO: make faster
				pick = []
				
				Models::Club.each do |cl|
					pick.push(cl) if cl.users.include? user.vk_id
				end
				
				return pick
			end
			
			# join the club
			def join_club(club, user, state)
				if not state
					club.users.delete(user.vk_id)
					club.founders.delete(user.vk_id)
				else
					club.users.push(user.vk_id) if not club.users.include? user.vk_id
				end
				
				club.save
			end
			
			# get all club members as objects
			def get_club_user_list(club)
				# TODO: make faster
				pick = []
				
				Models::User.each do |user|
					pick.push(user) if club.users.include? user.vk_id
				end
				
				return pick
			end
		
			configure do
				set :root, CT::AUTH_ROOT
			end
		
			get '/join' do
				user = get_user_for_current_session(request)
				locals = { :is_signed_in => user.nil?.opposite,
					 		  :user_role => (user.nil? ? :regular : user.role),
							  :allowed_admin_roles => ALLOWED_ADMIN_ROLES
						  	}
				
				erb :join_picker, :layout => :mini, :locals => locals
			end

			before ['/manage', '/clubs', '/partyform', '/ideas', '/make_idea'] do
				begin
					session = Session.existing_token(request.cookies["CT_AUTH"])
					raise("Session expired.") if not session.valid?
					
					if is_blacklisted? session.is_actually
						redirect(to('/blacklisted'))
					end
				rescue
					redirect(to('/continue'))
				end
			end
			
			get '/ideas' do
				all_ideas = CT::Models::Idea.all.to_a.sort_by { |item| item.when_added.to_i }
				all_ideas.reverse!
				 
				locals = { :all_ideas => all_ideas,
					 	   :current_user_id => get_user_for_current_session(request).vk_id }
				
				erb :ideas, :layout => :mini, :locals => locals 
			end
			
			get '/clubs' do
				user = get_user_for_current_session(request)
				
				if params[:id].nil? 
					erb :my_clubs, :layout => :mini, :locals => { :all_clubs => detect_clubs_for_user(user),
																  :user_id => user.vk_id }
				elsif params[:id] == '__new'
					# new club request
					club = Models::Club.new
					club.name = "Новый клуб #{DateTime.now.strftime('%d.%m.%Y %H:%M:%S')}"
					club.founders = [ user.vk_id ]
					club.users = [ user.vk_id ]
					club.join_type = :closed
					club.hidden = false
					club.save
					
					# redirect to the admin panel now
					identifier = club._id.to_s
					redirect(to('/clubs?id=' + identifier))
				else
					club = Models::Club.where( :_id => params[:id] ).first
					halt(404) if club.nil?
					
					if club.founders.include?(user.vk_id)
						erb :manage_club, :layout => :mini, :locals => { :club => club,
																		 :members => get_club_user_list(club) }
					elsif club.users.include?(user.vk_id)
						if club.join_type == :contact_link
							redirect(club.join_link.to_s) # redirect to the link
						else
							redirect('/clubs/' + club._id.to_s)
						end
					else
						join_club(club, user, true)
						redirect('/clubs/' + club._id.to_s)
					end
				end
			end
			
			post '/clubs' do
				user = get_user_for_current_session(request)
				halt(403) if user.nil?
				halt(400) if request.content_type != 'application/json'
				
				payload = JSON.parse(request.body.read).symbolize_keys
				club_id = payload[:id].to_s
				club = Models::Club.where( :_id => club_id ).first
				
				halt(404) if club.nil?
				halt(401) if not club.founders.include? user.vk_id
				
				case payload[:action]
				when 'save_params'
					club.description = payload[:description]
					club.name = payload[:name]
					club.hidden = payload[:hidden]
					club.join_link = payload[:join_link]
					club.join_type = if payload[:allow_registration]
										if payload[:join_link].to_s.size < 1
											:on_site
										else
											:contact_link
										end
									 else
										:closed
									 end
					club.save
				when 'change_icon'
					club.icon_url = payload[:value] || to('/images/nothumb.png')
					club.save
				when 'promote'
					who_id = payload[:who]
					halt(500) if who_id.nil? or not club.users.include? who_id
				
					club.founders.push(who_id)
					club.save
				when 'please_delete'
					club.delete
				else
					halt(500)
				end
			end
			
			post '/join_club' do
				user = get_user_for_current_session(request)
				halt(403) if user.nil?
				halt(400) if request.content_type != 'application/json'
				
				payload = JSON.parse(request.body.read).symbolize_keys
				club_id = payload[:id].to_s
				state = if payload[:state].is_a_boolean?
							payload[:state]
						else
							true
						end							
				
				club = Models::Club.where( :_id => club_id ).first
				halt(404) if club.nil?
				
				join_club(club, user, state)
			end
			
			# club join state
			get '/join_club' do
				user = get_user_for_current_session(request)
				club = Models::Club.where( :_id => params[:id] ).first
				
				content_type 'text/plain'
				if club.nil? or user.nil?
					"absent"
				elsif not club.users.include? user.vk_id
					"absent"
				elsif club.founders.include? user.vk_id
					"founder"
				else
					"member"
				end
			end
			
			get '/make_idea' do
				erb :ideas_new, :layout => :mini
			end
			
			post '/make_idea' do
				user = get_user_for_current_session(request)
				halt(403) if user.nil?
				
				all_contents_raw = request.body.read
				puts "raw original: #{all_contents_raw}"
				
				all_contents = all_contents_raw.force_encoding('utf-8')
				
				# autodetect heading and body
				all_contents_split = all_contents.split("\n").reject { |itm| itm.strip.size < 1 }
				heading = all_contents_split.shift
				description = all_contents_split.join("\n").gsub("<br>", "\n")
				
				# create new idea then
				idea = CT::Models::Idea.new
				idea.name = heading
				idea.description = description
				idea.author_id = user.vk_id
				idea.state = :brand_new
				idea.when_added = Time.now
				idea.support_ids = []
				idea.against_ids = []
				idea.save
				
				content_type 'text/plain'
				idea._id.to_s
			end
			
			post '/trigger_vote' do
				user = get_user_for_current_session(request)
				halt(403) if user.nil?
				
				halt(400) if request.content_type != 'application/json'
				payload = JSON.parse(request.body.read).symbolize_keys
				
				
			end
			
			get '/edit_article' do
				user = get_user_for_current_session(request)
				halt(404) if user.nil?
				halt(403) if not ALLOWED_ADMIN_ROLES.include? user.role
				
				article_id = params[:id] || '__new'
				
				article_object = CT::Models::NewsArticle.where( :_id => article_id ).first
				if article_object.nil?
					if article_id == '__new'
						# create a new one instead
						article_object = CT::Models::NewsArticle.new
						article_object.when_added = Time.now
						article_object.author_id = user.vk_id
						article_object.title = "Заголовок статьи"
						article_object.tagline = "Краткий смысл статьи"
						article_object.contents = "Саму статью пишите сюда..."
						article_object.save
						
						article_id = article_object._id.to_s
					else
						halt 404
					end
				end
				
				mcontents = ["# #{article_object.title}", "*#{article_object.tagline}*", article_object.contents.to_s]
				erb :edit_article, :locals => { :merged_contents => mcontents.join("\n"),
					 									  :article_id => article_id }
			end
			
			post '/edit_article' do
				halt(400) if request.content_type != 'application/json'
				
				user = get_user_for_current_session(request)
				halt(401) if user.nil? or not ALLOWED_ADMIN_ROLES.include? user.role
				
				payload = JSON.parse(request.body.read).symbolize_keys
				
				article_id = payload[:id]
				article_object = CT::Models::NewsArticle.where( :_id => article_id ).first
				
				halt(404) if article_object.nil?
				
				if payload.has_key?(:doDelete) and payload[:doDelete]
					# delete current article
					article_object.delete
				elsif payload.has_key? :contents
					# set update time
					article_object.when_updated = Time.now
					
					# componentize contents (title, tagline, the rest)
					contents = componentize_article_contents(payload[:contents])
				
					article_object.title = contents[:title]
					article_object.tagline = contents[:tagline]
					article_object.contents = contents[:contents]
					article_object.save
				else
					halt 400
				end
				
				content_type 'text/plain'
				"Success"
			end
			
			get '/blacklisted' do
				erb :blacklist_notice, :layout => :mini
			end

			get '/manage' do
				user = get_user_for_current_session(request)
				
				if not ALLOWED_ADMIN_ROLES.include? user.role
					redirect to('/join')
				else
					locals = { :current_user => user,
						 		  :all_users => CT::Models::User.all.to_a,
								  :all_news => CT::Models::NewsArticle.all.to_a,
							  	  :all_roles => ROLE_MAP }
					
					erb :admin, :layout => :mini, :locals => locals
				end
			end
			
			post '/manage' do
				user = get_user_for_current_session(request)
				halt(401) if not ALLOWED_ADMIN_ROLES.include? user.role
				
				halt(500) if request.content_type != 'application/json'
				
				payload = JSON.parse(request.body.read).symbolize_keys
				user_id = payload[:target]
				target_user = CT::Models::User.where( :vk_id => user_id ).first
				
				halt(500) if user_id.nil? or target_user.nil? or user.vk_id == user_id
				
				content_type 'text/plain'
				
				case payload[:what]
				when 'delete'
					blacklist_user(user_id)
					target_user.delete
				when 'changeRole'
					new_role = payload[:role] || 'regular'
					target_user.role = new_role.to_sym
					target_user.save
				when 'setApplicationStatus'
					status = payload[:status] || false
					puts payload
					
					halt(400) if not status.is_a_boolean?
					halt(403) if not target_user.application[:sent]
					
					if status
						puts "promote #{user_id}"
						target_user.role = :member # user promoted to a member
					else
						puts "reject #{user_id}"
						target_user.role = :regular
						target_user.application[:rejected] = true # application rejected
					end
					target_user.save
				else
					halt 500
				end
				
				"Success"
			end

			get '/partyform' do
				user = get_user_for_current_session(request)
				halt(500) if user.nil? 
				
				locals = { :user_status => user.role,
							  :user_full_name => user.full_name,
							  :user_page => user.page_link.html_friendly,
							  :user_application_sent => user.application[:sent],
						  	  :user_application_rejected => user.application[:rejected] }
			
				erb :join_form, :layout => :mini, :locals => locals
			end

			post '/partyform' do
				user = get_user_for_current_session(request)
				
				halt(500) unless request.content_type == 'application/json'
				payload = JSON.parse(request.body.read).symbolize_keys

				puts payload

				content_type 'text/plain'
				if user.nil?
					status 401
					"User doesn't exist"
				elsif user.application[:rejected]
					status 401
					"User's application already reviewed and rejected"
				elsif user.application[:sent]
					status 403
					"Application already sent from another window"
				else
					user.application = { :sent => true,
												:rejected => false,
												:name => payload[:name].to_s,
												:grade => payload[:grade].to_i,
												:why => payload[:description],
												:when => Time.now
											 }
					user.save
					"Success"

				end
			end

			get '/continue' do
				where = params[:tgt] || 'manage'
				if not params[:vaarg].nil?
					where = "#{where}?id=#{params[:vaarg]}"
				end

				begin
					session_access = Session.existing_token(request.cookies["CT_AUTH"] || "")
					redirect(to(where)) if session_access.valid?
				rescue => e
					puts e
				end
			
				erb :join_social, { :layout => :mini, :locals => { :target_page => where } }
			end

			post '/continue' do
				puts request.content_type
				halt(500) unless request.content_type == 'application/json'
			
				payload = JSON.parse(request.body.read).symbolize_keys
				puts payload

				session = nil
				session_diff_length = 2.hours
				auth_id = ''
				full_name = ''

				case payload[:type]
				when "vkauth"
					# format
					# { :id => 'vk_user_id',
					#   :expires => [UNIX timestamp when expires],
					#   :params => {
					#      "session" => {
					#          "mid" => 'vk_user_id',
					#          "sid" => 'VK server-side session token',
					#          "sig" => 'verification signature'
					#          "secret" => "oauth",
					#          "user" => {
					#               "id" => 'vk_user_id',
					#               "href" => 'VK page link',
					#               "first_name" => ...,
					#               "last_name" => ...
					#          }
					#      }
					#   }
					# }
					
					session_diff_length = payload[:expires] - Time.now.to_i
					auth_id = payload[:id]
					full_name = "#{payload[:params]['session']['user']['first_name']} #{payload[:params]['session']['user']['last_name']}"
				when "tgauth"
					# Telegram auth
					#
					# format: {
					#            :id => 'Telegram ID',
					#            :params => {
					#                 "first_name" => ...,
					#                 "last_name" => ...,
					#                 "hash" => "Telegram session hash for custom verification",
					#                 "username" => "@Telegram username",
					#                 "photo_url" => "Telegram photo url"
					#            }
					#         }

					auth_id = "tlg#{payload[:id]}"
					full_name = "#{payload[:params]['first_name']} #{payload[:params]['last_name']}"
				when "debugauth"
					halt(401) if not settings.environment == :development

					auth_id = "dbg#{payload[:id]}"
				else
					halt 400
				end

				# update user info
				update_user_info_during_auth(auth_id, payload[:type], payload)

				content_type 'text/plain'
				session = Session.new(auth_id, session_diff_length)
				session.token
			end

			get '/logout' do
				session = Session.existing_token(request.cookies["CT_AUTH"])
				if session.valid?
					user_id = session.is_actually
					session.finish
					
					if is_vk_user? user_id
						# call VK Open API JS logout callback
						erb :vk_perform_logout
					else
						redirect to('/continue')
					end
				else
					redirect to('/join')
				end
			end
		end
	end
end
