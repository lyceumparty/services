#!/usr/bin/env ruby
class Object
	def is_a_boolean?
		return [true, false].include? self
	end
end

class Hash
	def symbolize_keys
		result = {}
	
		self.each do |kv, vv|
			result[kv.to_s.to_sym] = vv
		end

		return result
	end
end

class String
	def is_italic?
		if self.start_with?('**')
			return false
		elsif self.start_with?('*') && self[-1] == '*'
			return true
		else
			return false
		end
	end
	
	def first_n_words(counter=5)
		words = self.split(' ')
		result = words[0...counter].join(' ')
		
		if result[-1] == '.' && result.size >= 2 && result[-2] != '.'
			result = "#{result}.." # TBC to be continued
		end
		
		return result
	end
	
   def html_friendly
      characters = { '<' => '&lt;',
                     '>' => '&gt;',
                     '@' => '&commat;' }
      
      result = self
      characters.each do |cc, cv|
         result.gsub! cc, cv
      end
      
      return result
   end
end

class TrueClass
   def opposite
      return false
   end
	
	def to_on_off
		return "on"
	end
end

class FalseClass
   def opposite
      return true
   end
	
	def to_on_off
		return "off"
	end
end