#!/usr/bin/env ruby
require 'sinatra'
require 'erubis'
require 'rdiscount'
require_relative 'common'

module CT
	module Apps
		class Promo < Sinatra::Base
			configure do
				# prepare environment
				set :root, CT::PROMO_COMPONENTS_ROOT
				set :app_map, CT::CONFIG['go_map']
			end

			get '/' do
				redirect to('/index')
			end
			
			get '/index' do
				erb :index, :layout => :template
			end
			
			get '/about' do
				erb :about, :layout => :template
			end
			
			get '/docs' do
				erb :docs, :layout => :template
			end
			
			get '/manifesto' do
				redirect to('/docs') # moved
			end
			
			get '/clubs' do
				erb :clubs, :layout => :template
			end
			
			get '/clubs/:id' do
				club = Models::Club.where( :_id => params['id'] ).first
				halt(404) if club.nil? # does not exist
				
				erb :club_page, :layout => :template, :locals => { :is_news => true, :club => club }
			end
			
			get '/news' do
				news_list = Models::NewsArticle.all.to_a.sort_by { |item| item.when_added.to_time.to_i }
				news_list.reverse!
				
				erb :news, :layout => :template, :locals => { :is_news => true,
															  :provided_news => news_list }
			end
			
			get '/news/:article_id' do
				article_id = params[:article_id]
				news_article = Models::NewsArticle.where( :_id => article_id ).first
				
				# article might be not found
				halt(404) if news_article.nil? or news_article.contents.nil?
				
				erb :news_article, :layout => :template, :locals => { :is_news => true,
																	  :article => news_article }
			end

			get '/go/:where' do
				where = params[:where].to_s
				halt(404) if not settings.app_map.has_key? where

				redirect settings.app_map[where]
			end

			not_found do
				status 404
				"404 not found"
			end
		end
	end
end

