CaveTown CMS

Requirements
============================
- A UNIX system
  - macOS 10.14 or newer for local debugging/development purposes
  - Debian Linux 10 or 11 for production
  - you can use any other UNIX-like OS as long as you know what you're doing
    and are able to set up the correct environment
- Ruby 2.4 or newer (2.7 recommended)
- MongoDB 4.4 or newer
- memcached 1.6.9 or newer

Quick start on a Mac
============================
I am writing this at 3 am and at the time of writing I still didn't make a 
proper Vagrant environment, so you'll have to do the VM part manually too.

Get a Debian Linux VM up and running in VMware Fusion, it shouldn't be that 
hard really.

Set up the VM:
$ sudo apt update && sudo apt upgrade -y

Set up memcached quickly:
$ sudo apt install memcached -y
$ sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/memcached.conf
$ sudo systemctl enable --now memcached

Set up MongoDB:
$ sudo apt install curl gnupg -y
$ curl -o mongod.deb https://repo.mongodb.org/apt/debian/dists/buster/mongodb-org/5.0/main/binary-amd64/mongodb-org-server_5.0.3_amd64.deb
$ sudo apt install -f ./mongod.deb
$ sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mongod.conf
$ sudo systemctl enable --now mongod

Now onto your Mac.

Check if you have Ruby installed. Please DON'T USE built-in system Ruby. 
Probably the world isn't going to end if you break your system Ruby installation
by accident, but reinstalling macOS, although easy, is still time consuming, so
just don't. Use rbenv, rvm or Homebrew to get Ruby or just compile it from 
source yourself.

Clone this repo:
$ git clone https://bitbucket.org/lyceumparty/services.git
$ cd services

Install all the dependencies:
$ bundle install

Then note down your Debian VM's IP address and set it accordingly in 'config.toml':
$ cp config.toml.template config.toml
$ open -e config.toml

Now you're good to go:
$ bundle exec rake

To simulate prod:
$ bundle exec rake prod


Production deployment
=========================

Do all the Debian VM steps from the above, then configure Ruby using default
apt repos:

$ sudo apt install ruby rubygems ruby-sinatra ruby-sinatra-contrib \
		   rake ruby-toml ruby-dev make gcc \
                   ruby-bundler ruby-erubis -y

Other deps still have to be installed manually:

$ git clone https://bitbucket.org/lyceumparty/services.git && cd services
$ sudo bundle install

Replace all hosts in 'config.toml' to 'localhost', since all the DB stuff is
running locally on the server now:

$ test -z "$EDITOR" && export EDITOR=nano
$ $EDITOR config.toml

Try running in prod directly:

$ bundle exec rake prod

Now set up user/group for the systemd service:

$ sudo /sbin/useradd -U -s /sbin/nologin -m -d /ct cavetown
$ cd ..
$ sudo mv -v services /ct/services
$ sudo chown -vR cavetown:cavetown /ct

Set up the systemd service itself:

$ sudo cp /ct/services/lyceumparty.service /etc/systemd/system/
$ sudo systemctl daemon-reload
$ sudo systemctl enable --now lyceumparty

